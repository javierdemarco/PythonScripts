# User Manual Python Excel Scripts

---

## Notes before usage

* Files have to be placed in the correct sub-directory: "Cobros" or "Facturas"
depending on what they are.
* Output will be in the correspondent sub-directory of output with out_{filename}
* It is necessary to create or have the Directories describe below:
    Files/
        Cobros/
            {CobrosFiles}
        Facturas/
            {FacturasFiles}
    Output/
        Cobros/
        Facturas/
    excelScript.py
    README.MD

## Python Necessary Modules

1. xlrd - Read Excel files
2. xlsxwriter - Create and Write into Excel files
3. datetime - Process and manage datetimes
4. os - Obtain system information
5. tkinter - Creates windows and button of the program

## Usage with Virtual Environment

1. Activate virtual environment to obtain all the necessary python modules for the script
    ```bash
    source excelScriptsEnv/bin/activate
    ```
2. Launch python script in root directory
    ```python
    pyhton excelScript.py
    ```
3. Your can process All files within both directory by just clicking in "Procesar Todos"
4. The other options
    1. In the window that will show up choose:
        1. The drop-menu will show all the files in Files/[Cobros, Facturas] directory.
4. Wait for the process to end, you will see a message above "Procesar" button.
5. Continue processing files in the directory or close the window.
6. You will see the output of the files in the Output directory, with the name out_{filename}

## Usage with system Python

1. Install all the necessary Modules in the list above with:
    ```bash
    pip install {ModuleName}
    ```
2. Launch python script in root directory
    ```python
    pyhton excelScript.py
    ```
3. Your can process All files within both directory by just clicking in "Procesar Todos"
4. The other options
    1. In the window that will show up choose:
        1. The drop-menu will show all the files in Files/[Cobros, Facturas] directory.
4. Wait for the process to end, you will see a message above "Procesar" button.
5. Continue processing files in the directory or close the window.
6. You will see the output of the files in the Output directory, with the name out_{filename}