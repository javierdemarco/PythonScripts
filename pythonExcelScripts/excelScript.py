import xlrd
import xlsxwriter
import datetime
import os
from tkinter import *
from tkinter.ttk import *

# Column titles
columnTitles = {
    "Movimiento" : 0,
    "Centro" : 1,
    "Fecha FRA" : 2,
    "Código FRA" : 3,
    "Fecha C/A" : 4,
    "Código C/A" : 5,
    "D" : 6,
    "Importe" : 7,
    "R" : 8,
    "Código Cliente" : 9,
    "Asiento" : 10,
    "Fecha Asiento" : 11,
    "Observaciones" : 12,
    "Descipción Error" : 13
}
# CONSTANTS
R_VALUE = "S"
D_VALUE = "E"
RV_VALUE_ALONE = 1
RV_VALUE_DZ_ZP = 3
RX_VALUE = 2
TITLE_ROW = 0
RV_TYPE = "RV"
DZ_TYPE = "DZ"
RX_TYPE = "RX"
ZP_TYPE = "ZP"
FILE_TYPES = ['Facturas', 'Cobros']
FACTURAS_DIR = "Files/Facturas"
COBROS_DIR = "Files/Cobros"
OUTPUT_DIR = "Output"


def processRVorRX(workBookRead, workSheetRead, workSheetWrite,
                  columnsRead, row, rowWrite):
    '''
    processRVorRX - Process RV and RX movements, writing it in another Excel
                    containing the necesary information about them.

    Returns:
        Void
    '''
    # Movement Code
    if (workSheetRead.cell_value(row, columnsRead['Document Type']) == RX_TYPE):
        workSheetWrite.write(rowWrite, columnTitles['Movimiento'], RX_VALUE)
    elif (workSheetRead.cell_value(row, columnsRead['Document Type']) == RV_TYPE):
        workSheetWrite.write(rowWrite, columnTitles['Movimiento'], RV_VALUE_ALONE)
    # Place Code
    workSheetWrite.write(rowWrite, columnTitles['Centro'],
                            workSheetRead.cell_value(row, columnsRead['Account']))
    # Date F/A
    date = xlrd.xldate_as_tuple(workSheetRead.cell_value(row, columnsRead['Document Date']),
                                workBookRead.datemode)
    date = datetime.datetime(*date)
    workSheetWrite.write(rowWrite, columnTitles['Fecha FRA'],
                            "{}".format(date.strftime("%Y%m%d")))
    # FRA Code
    workSheetWrite.write(rowWrite, columnTitles['Código FRA'],
                            workSheetRead.cell_value(row, columnsRead['Invoice reference']))
    if (workSheetRead.cell_value(row, columnsRead['Document Type']) == RX_TYPE):
        # C/A Date For RX
        date = xlrd.xldate_as_tuple(
            workSheetRead.cell_value(row, columnsRead['Payment date']),
            workBookRead.datemode)
        date = datetime.datetime(*date)
        workSheetWrite.write(rowWrite, columnTitles['Fecha C/A'],
                            "{}".format(date.strftime("%Y%m%d")))
        # C/A Code For RX
        workSheetWrite.write(rowWrite,
                            columnTitles['Código C/A'],
                            workSheetRead.cell_value(row, columnsRead['Invoice reference']))
    # D
    workSheetWrite.write(rowWrite, columnTitles['D'], D_VALUE)
    # Charge Amount
    fee = workSheetRead.cell_value(row, columnsRead['Amount in doc. curr.'])
    fee = abs(fee)
    workSheetWrite.write(rowWrite, columnTitles['Importe'], '{}'.format(fee).replace('.', ','))
    # R
    workSheetWrite.write(rowWrite, columnTitles['R'], R_VALUE)


def processDZOrZP(workBookRead, workSheetRead, workSheetWrite, columnsRead,
                  clearingDocumentList, accountList, rowRead, rowWrite):
    '''
    processDZOrZP - Process DZ and ZP movements, writing it in another Excel
                    containing the necesary information about them.

    Returns:
        Void
    '''
    # Filter to see what columns are the ones containing the clearing document
    clearingDocument = workSheetRead.cell_value(rowRead, columnsRead['Clearing Document'])
    accountFilter = workSheetRead.cell_value(rowRead, columnsRead['Account'])
    filterClDoc = [i for i in range(len(clearingDocumentList))
                   if clearingDocumentList[i]==clearingDocument and accountList[i]==accountFilter]
    for rowFilter in filterClDoc:
        if workSheetRead.cell_value(rowFilter, columnsRead['Document Type']) == RV_TYPE:
            # Movement Code
            workSheetWrite.write(rowWrite, columnTitles['Movimiento'], RV_VALUE_DZ_ZP)
            # Place Code
            workSheetWrite.write(rowWrite, columnTitles['Centro'],
                                 workSheetRead.cell_value(rowFilter, columnsRead['Account']))
            # F/A Date
            date = xlrd.xldate_as_tuple(
                workSheetRead.cell_value(rowFilter, columnsRead['Document Date']),
                workBookRead.datemode)
            date = datetime.datetime(*date)
            workSheetWrite.write(rowWrite, columnTitles['Fecha FRA'],
                                 "{}".format(date.strftime("%Y%m%d")))
            # FRA Code
            workSheetWrite.write(rowWrite,
                                 columnTitles['Código FRA'],
                                 workSheetRead.cell_value(rowFilter, columnsRead['Document Number']))
            # C/A Date From DZ
            date = xlrd.xldate_as_tuple(
                workSheetRead.cell_value(rowRead, columnsRead['Payment date']),
                workBookRead.datemode)
            date = datetime.datetime(*date)
            workSheetWrite.write(rowWrite, columnTitles['Fecha C/A'],
                                "{}".format(date.strftime("%Y%m%d")))
            # C/A Code From DZ
            workSheetWrite.write(rowWrite,
                                columnTitles['Código C/A'],
                                workSheetRead.cell_value(rowRead, columnsRead['Invoice reference']))
            # D
            workSheetWrite.write(rowWrite, columnTitles['D'], D_VALUE)
            # Charge Amount
            fee = workSheetRead.cell_value(rowFilter, columnsRead['Amount in doc. curr.'])
            fee = abs(fee)
            workSheetWrite.write(rowWrite, columnTitles['Importe'],
                                 '{}'.format(fee).replace('.', ','))
            # R
            workSheetWrite.write(rowWrite, columnTitles['R'], R_VALUE)
            rowWrite+=1
    return rowWrite


def processOneFile(fileName):
    '''
    processOneFile - Reads an Excel file and calls the correct method to process
                     each line.

    Returns:
        Void
    '''
    try:
        columnsRead = dict()
        rowWrite = 1
        if fileName in onlyfilesFacturas:
            excelFile = (FACTURAS_DIR + "/{}".format(fileName))
            fileType = FILE_TYPES[0]
        elif fileName in onlyfilesCobros:
            excelFile = (COBROS_DIR + "/{}".format(fileName))
            fileType = FILE_TYPES[1]
        # Workbook to Read and extract data
        workBookRead = xlrd.open_workbook(excelFile)
        workSheetRead = workBookRead.sheet_by_index(0)
        # Workbook to create and Write
        workBookWrite = xlsxwriter.Workbook(OUTPUT_DIR + "/" + fileType + '/out_{}'.format(fileName))
        workSheetWrite = workBookWrite.add_worksheet()
        # Print Columns in excel
        for title in columnTitles:
            workSheetWrite.write(TITLE_ROW, columnTitles[title], title)
        # Create the dictionary of the columns read in the Excel file
        for column in range(workSheetRead.ncols):
            columnsRead["{}".format(workSheetRead.cell_value(TITLE_ROW, column))] = column
        if fileType == FILE_TYPES[0]:
            for row in range(workSheetRead.nrows):
                # Main Loop
                if row == TITLE_ROW or row == workSheetRead.nrows-1:
                    continue
                processRVorRX(workBookRead, workSheetRead, workSheetWrite,
                                                columnsRead, row, rowWrite)
                rowWrite+=1
        elif fileType == FILE_TYPES[1]:
            # Obtain all the clearing document numbers to filter by afterwards
            clearingDocumentList = workSheetRead.col_values(columnsRead['Clearing Document'])
            clearingDocumentList[0] = '0'
            accountList = workSheetRead.col_values(columnsRead['Account'])
            accountList[0] = '0'
            for row in range(workSheetRead.nrows):
                if row == TITLE_ROW or row == workSheetRead.nrows-1:
                    continue
                # DZ and ZP process
                if (workSheetRead.cell_value(row, columnsRead['Document Type']) == DZ_TYPE or
                    workSheetRead.cell_value(row, columnsRead['Document Type']) == ZP_TYPE):
                    rowWrite = processDZOrZP(workBookRead, workSheetRead, workSheetWrite,
                                             columnsRead, clearingDocumentList, accountList, row,
                                             rowWrite)
                # RX Process
                elif (workSheetRead.cell_value(row, columnsRead['Document Type']) == RX_TYPE):
                    processRVorRX(workBookRead, workSheetRead, workSheetWrite,
                                                columnsRead, row, rowWrite)
                    rowWrite+=1
    except Exception as identifier:
        print("Ha habido un error procesando: " + identifier)
        pass
    workBookWrite.close()
    lblProcessCompleted = Label(window, text="Procesamiento Completado, puede cerrar la ventana")
    lblProcessCompleted.grid(column=0, row=4)


def btnProcessClicked():
    '''
    btnProcessClicked - method callback of Process file Button

    Returns:
        Void
    '''
    filename = fileNameCombo.get()
    processOneFile(filename)


def btnProcessAllClicked():
    '''
    btnProcessAllClicked - method callback of Process all files Button

    Returns:
        Void
    '''
    for item in onlyfilesCobros:
        processOneFile(item)
    for item in onlyfilesFacturas:
        processOneFile(item)


if __name__== "__main__":
    '''
    Main - This Program process Excel files with a certain format and creates
            the output of that file with an especific format containing the necesary
            information regarding the first Excel.
    '''
    # Window Tkinter
    window = Tk()
    window.title("Procesamiento de Docs Excel")
    window.geometry('400x150')
    # Labels
    lblFile = Label(window, text="Introduce el nombre del fichero:")
    lblFile.grid(row=0)
    # Buttons
    btnProcess = Button(window, text="Procesar", command=btnProcessClicked)
    btnProcess.grid(row=5)
    btnProcessAll = Button(window, text="Procesar Todos", command=btnProcessAllClicked)
    btnProcessAll.grid(row=6)
    # ComboBoxes
    fileNameCombo = Combobox(window, width=30)
    # Regex exp to filter temp files
    # Files in directory excelFiles
    onlyfilesFacturas = [f for f in os.listdir(FACTURAS_DIR)
                 if os.path.isfile(os.path.join(FACTURAS_DIR, f))
                 and not f.startswith('.')]
    onlyfilesCobros = [f for f in os.listdir(COBROS_DIR)
                       if os.path.isfile(os.path.join(COBROS_DIR, f))
                       and not f.startswith('.')]
    fileNameCombo['values'] = onlyfilesCobros + onlyfilesFacturas
    fileNameCombo.grid(row=3)
    window.mainloop()
