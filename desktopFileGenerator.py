#!/usr/bin/python3
"""
  Desktop File Generator for AppImages
"""
import os
import PySimpleGUI as sg


def generate_desktop_file(ex_path, name, icon_path):
    f = open(name, "w+")
    f.write("[Desktop Entry]\n")
    f.write("Encoding=UTF-8\n")
    f.write("Version=1.0\n")
    f.write("Type=Application\n")
    f.write("Terminal=false\n")
    f.write("Exec=" + ex_path + "\n")
    f.write("Name=" + name + "\n")
    f.write("Icon=" + icon_path + "\n")
    f.close()
    os.rename(f.name, "/usr/share/applications/" + f.name + ".desktop")


layout = [[sg.Text('Exec Path'), sg.InputText()],
          [sg.Text('Name'), sg.InputText()],
          [sg.Text('Icon Path'), sg.InputText()],
          [sg.Button('OK'), sg.Button('Cancel')]]

window = sg.Window("Hello World", layout)


while True:
    event, values = window.read()
    if event in (sg.WIN_CLOSED, 'Cancel'):
        break
    generate_desktop_file(values[0], values[1], values[2])
    break

window.close()
